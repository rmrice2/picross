package picross.model;
import java.io.Serializable;

public class Puzzle implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int[][] map;
	                //playerMap;
	private String name;
	private int height, width, id, magicNumber;
	private boolean finished = false,
			        inProgress = false;
	
	public Puzzle(String name, int width, int height, int id, String bitstring){
		
		this.name = name;
		this.width = width;
		this.height = height;
		this.id = id;
		
		this.map = new int[height][width];

		//populate the game map
		int cursor = 0;
		magicNumber = 0;
		
		for(int i = 0; i < height; i++){
			for(int j = 0; j < width; j++){
				map[i][j] = Character.digit(bitstring.charAt(cursor), 10);
				
				if(map[i][j] == 1){
					magicNumber++;
				}
				cursor++;
			}
		}
	}
	
	public int getHeight(){
		return height;
	}
	
	public int getWidth(){
		return width;
	}
	
	public int getMagicNumber(){
		return magicNumber;
	}
	
	public int getID(){
		return id;
	}
	
	public int[][] getMap(){
		return map;
	}
	
	public String getName(){
		return name;
	}
	
	public boolean getFinished(){
		return finished;
	}
	
	public void setFinished(boolean finished){
		this.finished = finished;
	}
}
