package picross.model;

import javafx.scene.layout.Pane;

public class Cell extends Pane{

	private final int FILL = 1, BLANK = 0;
	private int row, col;
	private int currentStatus;
	private String styleEmpty, styleFill, styleX;
	
	public Cell(int row, int col){
		super();
		
		this.row = row;
		this.col = col;
		
		String borderStyle;
		if((row + 1) % 5 == 0 && (col + 1) % 5 == 0){
			borderStyle = "-fx-border-width: 1 2 2 1;";
		}
		else if((row + 1) % 5 == 0){
			borderStyle = "-fx-border-width: 1 1 2 1;";
		}
		else if((col + 1) % 5 == 0){
			borderStyle = "-fx-border-width: 1 2 1 1;";
		}
		else{
			borderStyle = "-fx-border-width: 1 1 1 1;";
		}
		
		styleEmpty = "-fx-border-color:blue; " + borderStyle + "-fx-background-color:white;";
		styleFill = "-fx-border-color:blue; " + borderStyle + "-fx-background-color:#222222;";
		styleX = "-fx-border-color:blue; " + borderStyle + "-fx-background-color:#bbbbbb;";

		setStyle(styleEmpty);
		currentStatus = BLANK;
	}
	
	public int getRow(){
		return row;
	}
	
	public int getCol(){
		return col;
	}
	
	public void setStatus(int status){
		currentStatus = status;
		
		if(status == BLANK){
			setStyle(styleEmpty);
		}
		else if(status == FILL){
			setStyle(styleFill);
		}
		else{
			setStyle(styleX);
		}
	}
	
	public int getStatus(){
		return currentStatus;
	}
}
