package picross.model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class PuzzleMaker {
	public static void write(ArrayList<Puzzle> list){
		try{
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("puzzles.dat"));
			oos.writeObject(list);
			oos.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public static ArrayList<Puzzle> read(){
		ArrayList<Puzzle> list;
		try{
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream("puzzles.dat"));
			list = (ArrayList<Puzzle>)ois.readObject();
			ois.close();
			return list;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static void main(String[] args){
		int id = 0;
		Puzzle puzz = new Puzzle("Cherries", 10, 10, 0, "0000111111000000010000000011000000110100011100010010111011101111110111111111111101110111110000001110");		 
		
		ArrayList<Puzzle> puzzList = new ArrayList<Puzzle>();
		puzzList.add(puzz);
		
		puzz = new Puzzle("Black Cat", 10, 10, 1, "0010100000011110000010111000101111100100011100011000111100110011111001011111100101101110111101111110");		
		puzzList.add(puzz);
		
		puzz = new Puzzle("Fish", 10, 10, 2, "0000100000000001100010001111001101111110011111111101111111111101111110100011110000000110000000100000");
		puzzList.add(puzz);
		
		puzz = new Puzzle("test", 10, 10, 3, "1000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000");
		puzzList.add(puzz);
		
		puzz = new Puzzle("weird", 10, 10, 4, ""
				+ "1111111111"
				+ "0000000001"
				+ "1000000001"
				+ "0000000001"
				+ "1000000001"
				+ "0000000001"
				+ "1000000001"
				+ "0000000001"
				+ "1000000001"
				+ "0101010101");
		puzzList.add(puzz);
		
		write(puzzList);
		
		//ArrayList<Puzzle> hi = read();
		
		
	}
}
