package picross.model;

import javafx.scene.control.Label;
import javafx.scene.paint.Color;

public class Hint extends Label {

	public int rangeStart, rangeEnd, hint;
	public boolean isGrey = false;
	String regexL, regexR;
	
	public Hint(int hint, int start, int end){
		super();
		this.setText(Integer.toString(hint));
		
		this.hint = hint;
		this.rangeStart = start;
		this.rangeEnd = end;
	}
	
	public void setGrey(){
		isGrey = true;
		this.setTextFill(Color.GRAY);
	}
	
	public void setBlack(){
		isGrey = false;
		this.setTextFill(Color.BLACK);
	}
}
