package picross.model;
public class Group {

	int length, start, end;
	boolean bounded;
	
	public Group(int length, int start, boolean bounded){
		this.length = length;
		this.start = start;
		this.bounded = bounded;

		end = start + length - 1;
	}
	
	
}
