package picross.model;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import picross.view.*;

public class PuzzleApp extends Application{
	
	Stage mainStage;
	Scene menuScene;
	MenuController menuController;

	public void start(Stage mainStage) throws Exception{
		this.mainStage = mainStage;
		
		startMenu();
	}
	
	public void startMenu(){
		try {
			FXMLLoader loader = new FXMLLoader();
			
			loader.setLocation(getClass().getResource("/picross/view/mainmenu.fxml"));
			
			AnchorPane root;
			
			root = (AnchorPane)loader.load();
			menuController = loader.getController();
			
			menuController.start(this);
			
			menuScene = new Scene(root);
			mainStage.setScene(menuScene);
			mainStage.setTitle("Picross");
			mainStage.show();

		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public void refreshMenu(){
		menuController.refresh();
		mainStage.setScene(menuScene);
		mainStage.setTitle("Picross");
		mainStage.show();
	}
	
	public void startPuzzle(Puzzle puzzle){
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/picross/view/puzzle.fxml"));
		
		AnchorPane root;
		try {
			root = (AnchorPane)loader.load();
		
			PuzzleController controller = loader.getController();
		
			controller.start(this, puzzle);
		
			Scene scene = new Scene(root);
			mainStage.setScene(scene);
			mainStage.setTitle("Picross");
			mainStage.show();
		}
		catch (IOException e) {
			System.out.println("Error loading FXML");
			e.printStackTrace();
			System.exit(0);
		}
		
	}
	
	public static void main(String[] args){
		launch(args);
	}
}
