package picross.model;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

public class Line extends GridPane {

	private static final int ROW = 0;
	
	private int type,
	            size,
	            maxHintNum;
	private int[] correctLine;
	private int[] currentLine;
	public int correctNumFills;
	private int currentNumFills;
	private Hint[] hints;
	private String regex;
	
	int gridWidth, gridHeight;

	public Line(int type, int size, int[] correctLine){
		this.type = type;
		this.size = size;
		this.correctLine = correctLine;
		this.currentLine = new int[size];
		
		ArrayList<Group> g = parseLine(correctLine);
		correctNumFills = currentNumFills;
		currentNumFills = 0;
		initHints(g);
		addHints();
	}
	
	public void addHints(){
		
		maxHintNum = 5;
		 
		for(int k = hints.length - 1; k >= 0; k--){			
			if(type == ROW){
				this.add(hints[k], k + maxHintNum - hints.length, 0);
			}
			else{
				this.add(hints[k], 0, k + maxHintNum - hints.length);
			}
		}
	}
	
	//this method finds every contiguous group of filled squares in the line
	private ArrayList<Group> parseLine(int[] line){
		
		ArrayList<Group> groups = new ArrayList<>();
		int currType, prevType = 2, length = 0, numFills = 0;
		
		for(int i = 0; i < line.length; i++){
			currType = line[i];
			
			//found the start of a grouping
			if(currType == 1){
				numFills++;
				length++;
				if(i == line.length - 1){
					if(i - length - 1 < 0 || line[i - length] == 2){
						groups.add(new Group(length, i - length + 1, true));
					}
					else{
						groups.add(new Group(length, i - length + 1, false));
					}
				}
			}
			//found the end of a grouping
			else if(prevType == 1){
				if(currType == 2 && ((i - length - 1) < 0 || line[i - length - 1] == 2)){
					groups.add(new Group(length, i - length, true));
				}
				else{
					groups.add(new Group(length, i - length, false));
				}
				length = 0;
			}
			prevType = currType; 
		}
		
		currentNumFills = numFills;
		return groups;
	}

	private void initHints(ArrayList<Group> hintArray){
		
		if(hintArray.isEmpty()){
			hints = new Hint[1];
			hints[0] = new Hint(0, 0, size - 1);
			regex = "(0|2){" + size + "}";
			hints[0].setTextFill(Color.GRAY);
			return;
		}
		
		String regexL, regexR;
		hints = new Hint[hintArray.size()];
		regex = "(0|2)*";
		
		int leftSum = 0;
		
		for(int i = 0; i < hintArray.size(); i++){
			int rightSum = 0;
			regexL = regex;
			regex += "(0|1){" + hintArray.get(i).length + "}(0|2)+";
			regexR = "(0|2)*";
			
			for(int j = i+1; j < hintArray.size(); j++){
				regexR += "(0|1){" + hintArray.get(j).length + "}(0|2)+";
				rightSum += hintArray.get(j).length + 1;
			}
			
			hints[i] = new Hint(hintArray.get(i).length, leftSum, size - rightSum - 1);
			hints[i].regexL = regexL.substring(0, regexL.length() - 1) + "*";
			hints[i].regexR = regexR.substring(0, regexR.length() - 1) + "*";
			
			leftSum += hintArray.get(i).length + 1;
		}
		
		regex = regex.substring(0, regex.length() - 1) + "*";
	}
	
	public int getSquare(int index){
		return currentLine[index];
	}
	
	public void setSquare(int index, int status){		
		currentLine[index] = status;
		updateGreys();
	}
	
	public boolean checkCorrectness(){
		boolean isCorrect = true;
		
		for(int i = 0; i < size && isCorrect; i++){
			if(correctLine[i] == currentLine[i] || (correctLine[i] == 0 && currentLine[i] == 2)){
				isCorrect = true;
			}
			else{
				isCorrect = false;
			}
		}
		
		return isCorrect;
		
	}
	
	public void blackout(){
		for(int i = 0; i < hints.length; i++){
			hints[i].setBlack();
		}
	}
	
	public void greyout(){
		for(int i = 0; i < hints.length; i++){
			hints[i].setGrey();
		}
	}
		
	public void updateGreys(){
	
		String lineString = "";
		ArrayList<Group> groups = parseLine(currentLine);
		
		//check for illogical row
		if(currentNumFills > correctNumFills){
			blackout();
			return;
		}
		
		for(int i = 0; i < currentLine.length; i++){
			lineString += currentLine[i] + "";
		}

		//check for a completed row
		if(currentNumFills == correctNumFills){
			Pattern p = Pattern.compile(regex);
			Matcher m = p.matcher(lineString);

			if(m.matches()){
				greyout();
				return;
			}
		}
		
		for(int i = 0; i < hints.length; i++){
			int matchCount = 0;
			//int matchIndex = -1;
			Hint hint = hints[i];
			
			for(int j = 0; j < groups.size(); j++){
				Group group = groups.get(j);
		
				//check if the group is within the current hint's range, if they are the same length,
				//and if the hint is bounded at both ends
				if(group.start >= hint.rangeStart && group.end <= hint.rangeEnd && group.length == hint.hint && group.bounded){				
					boolean matchesL = false, matchesR = false;
					
					//first we check the squares to the left of the current group against the 
					//regex pattern representing the hints left of the current hint
					String left = lineString.substring(0, group.start);
					Pattern pLeft = Pattern.compile(hint.regexL);
					Matcher mLeft = pLeft.matcher(left);
					
					matchesL = mLeft.matches();
					
					//if left matches, do the same check on the right
					if(matchesL){
						String right = lineString.substring(group.end + 1);
						Pattern pRight = Pattern.compile(hint.regexR);
						Matcher mRight = pRight.matcher(right);
						
						matchesR = mRight.matches();
					}
					
					//if both the right and left match, then the group MAY be a match of the hint
					if(matchesL && matchesR){
						matchCount++;
					}
				}
			}
			
			//if in the process of checking each hint against the group, exactly one hint matched,
			//then that hint is unambiguously matched, and can be greyed out.
			if(matchCount == 1){
				hint.setGrey();
			}
			else{
				hint.setBlack();
			}
		}
	}

}
