package picross.view;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import picross.model.Puzzle;
import picross.model.PuzzleApp;

public class MenuController {

	private final int ICONSIZE = 80;
	
	@FXML TilePane tilePane;
	@FXML Button button5;
	@FXML Button button10;
	@FXML Button button15;
	
	PuzzleApp main;
	ArrayList<Puzzle> puzzles;
	
	public void start(PuzzleApp main){
		this.main = main;
		
		puzzles = read();

		tilePane.setStyle("-fx-padding: 10px");
		
		refresh();
	}
	
	public void refresh(){
		System.out.println("loading new puzzles");
		tilePane.getChildren().clear();
		
		for(int i = 0; i < puzzles.size(); i++){
			VBox tileContainer = new VBox();
			
			tileContainer.setStyle("-fx-padding: 8px");
			
			Image icon;
			ImageView tile;
			Label title;
			
			System.out.println(puzzles.get(i).getName() + ", " + puzzles.get(i).getFinished());
			
			if(!puzzles.get(i).getFinished()){
				icon = new Image("file:images/qmark.png", ICONSIZE, ICONSIZE, true, true);
				title = new Label("Puzzle #" + (i + 1));
			}
			else{
				System.out.println("puzzle" + i + ".png");
				icon = new Image("file:images/puzzle" + i + ".png", ICONSIZE, ICONSIZE, true, true);
				title = new Label(puzzles.get(i).getName());
			}

			tile = new ImageView(icon);
			tileContainer.getChildren().add(tile);
			tileContainer.getChildren().add(title);
			
			tile.setUserData(i); //tag the tile with the appropriate puzzle id
			
			tile.setOnMouseClicked((MouseEvent event) -> {
				ImageView selected = (ImageView)event.getSource();
				
				int puzzID = (int)selected.getUserData(); //fetch the puzzle id from the selected tile
				Puzzle puzz = puzzles.get(puzzID);

				main.startPuzzle(puzz);
			});
			
			tilePane.getChildren().add(tileContainer);
		}
	}
	
	public static void write(ArrayList<Puzzle> puzzle){
		try{
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("puzzles.dat"));
			oos.writeObject(puzzle);
			oos.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public static ArrayList<Puzzle> read(){
		ArrayList<Puzzle> puzz;
		try{
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream("puzzles.dat"));
			puzz = (ArrayList<Puzzle>)ois.readObject();
			ois.close();
			
			return puzz;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return null;
	}
}
