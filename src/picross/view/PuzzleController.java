package picross.view;

import java.text.SimpleDateFormat;
import java.util.Date;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import picross.model.*;

public class PuzzleController {

	private final int CELL_SIZE = 25;
	private final int ROW = 0, COL = 1;
	private final int BLANK = 0, FILL = 1, X = 2;
	
	@FXML VBox rowPanel;      //this panel is located on the left & holds the hint panels for the rows
	@FXML HBox colPanel;      //this panel is located on the top & holds the hint panels for the columns
	@FXML GridPane gameGrid;  //this is the main grid
	@FXML Button back;
	@FXML Label timerLabel;
	@FXML ImageView image;
	
	private PuzzleApp main;
	private Line[] rowHints, colHints;  //keeps track of the internal logic of each line
	private Cell[][] squares;
	private Puzzle puzzle;
	
	private int height,
				width,
				maxHintNumR = 5,
				maxHintNumC = 5,
				draggingFrom = -1,
				fillCounter = 0;
	
	private boolean gameWon = false;
	
	public void start(PuzzleApp main, Puzzle puzzle){
				
		this.main = main;
		this.puzzle = puzzle;
		this.height = puzzle.getHeight();
		this.width = puzzle.getWidth();
		
		squares = new Cell[height][width];
		rowHints = new Line[height];
		colHints = new Line[width];

		rowPanel.setPrefHeight(height * CELL_SIZE);
		rowPanel.setPrefWidth(maxHintNumR * CELL_SIZE);
		
		colPanel.setPrefWidth(width * CELL_SIZE);
		colPanel.setPrefHeight(maxHintNumC * CELL_SIZE);
		
		//create the left panel, top panel, and game grid
		setupLines();
		setupRowPanel();
		setupColPanel();
		setupSquares();
		setupTimer();
	}
	
	public void setupLines(){
		int[][] map = puzzle.getMap();
		
		for(int i = 0; i < height; i++){
			rowHints[i] = new Line(ROW, width, map[i]);
		}
		
		//fetch each column from map to populate the column lines
		for(int i = 0; i < width; i++){
			int[] temp = new int[height];
			
			for(int j = 0; j < height; j++){
				temp[j] = map[j][i];
			}
			
			colHints[i] = new Line(COL, height, temp);
		}
	}
	
	public void backToMenu(){
		main.refreshMenu();
	}
	
	public void setupRowPanel(){
		//populate rowPanel with rowClues
		for(int i = 0; i < height; i++){
			rowHints[i].setAlignment(Pos.CENTER);
			rowHints[i].setPrefHeight(CELL_SIZE);
			rowHints[i].setStyle("-fx-background-color:white");
			
			for(int j = 0; j < maxHintNumR; j++){
				ColumnConstraints colConstraints = new ColumnConstraints();
				colConstraints.setHalignment(HPos.CENTER);
				colConstraints.setPercentWidth(33);
				rowHints[i].getColumnConstraints().add(colConstraints);
			}
			
			rowPanel.getChildren().add(rowHints[i]);
		}
	}
	
	public void setupColPanel(){
		//populate colPanel with colClues
		for(int i = 0; i < width; i++){

			colHints[i].setAlignment(Pos.CENTER);
			colHints[i].setPrefWidth(CELL_SIZE);
			colHints[i].setStyle("-fx-background-color:white");
	
			for(int j = 0; j < maxHintNumC; j++){
				RowConstraints rowConstraints = new RowConstraints();
				rowConstraints.setValignment(VPos.BOTTOM);
				rowConstraints.setPercentHeight(33);
				colHints[i].getRowConstraints().add(rowConstraints);
			}
			
			colPanel.getChildren().add(colHints[i]);
		}
	}

	//this method adds squares to the game grid, and adds mouse listeners to each square
	public void setupSquares(){
		for(int i = 0; i < height; i++){
			for(int j = 0; j < width; j++){
				squares[i][j] = new Cell(i,j);

				//~~MOUSE EVENT LISTENERS~~
				
				//activates when the user presses a mouse button
				squares[i][j].setOnMousePressed((MouseEvent event) -> {
					Cell square = (Cell)event.getSource();
					int r = square.getRow();
					int c = square.getCol();
					
					//left click: blank -> fill, fill -> blank, x -> blank
					if(event.getButton() == MouseButton.PRIMARY){
						leftClick(square);
						updateHints(r,c);
					}
					//right click: blank -> x, fill -> blank, x -> blank
					else if(event.getButton() == MouseButton.SECONDARY){
						rightClick(square);
						updateHints(r,c);
					}
				});
				
				//activates when the user hovers the mouse over a square
				squares[i][j].setOnMouseEntered((MouseEvent event) -> {
					highlight((Cell)event.getSource());
				});
				
				//activates when the user moves the mouse away from a square
				squares[i][j].setOnMouseExited((MouseEvent event) -> {
					unhighlight((Cell)event.getSource());
				});
				
				//activates if the user moves the mouse while holding down the mouse button
				squares[i][j].setOnDragDetected((MouseEvent event) -> {
					Cell square = (Cell)event.getSource();
					square.startFullDrag();
					
					//keep a record of what type of square the user is dragging from, to distinguish between erasing squares and filling them in
					draggingFrom = square.getStatus(); 
				});
				
				//activates when the user moves the mouse into a new square with the mouse button held down
				squares[i][j].setOnMouseDragEntered((MouseEvent event) -> {
					Cell square = (Cell)event.getSource();
					int r = square.getRow();
					int c = square.getCol();

					highlight(square);
					
					//if dragging from blank, change all squares to blank
					if(draggingFrom == BLANK){
						if(square.getStatus() == FILL){
							fillCounter--;
						}
						square.setStatus(BLANK);
						updateHints(r,c);
					}
					//if dragging from X or from filled, change blank squares to X or to filled
					else if(draggingFrom != -1 && square.getStatus() == BLANK){
						square.setStatus(draggingFrom);
						if(draggingFrom == FILL){
							fillCounter++;
						}
						updateHints(r,c);
					}
					
				});
				
				squares[i][j].setOnMouseDragExited((MouseEvent event) -> {
					unhighlight((Cell)event.getSource());					
				});
				
				gameGrid.add(squares[i][j], j, i);
			}
		}
	}
	
	public void setupTimer(){
		long start = System.currentTimeMillis();
		timerLabel.setText("00:00");

		Timeline timeline = new Timeline(new KeyFrame(
				Duration.millis(1000),
				ae -> updateClock(start)));
		timeline.setCycleCount(Animation.INDEFINITE);
		timeline.play();
	}
	
	private void updateClock(long start){
		if(gameWon == true){
			return;
		}
		long elapsedTime = System.currentTimeMillis() - start;
		SimpleDateFormat sdf = new SimpleDateFormat("mm:ss");
		timerLabel.setText(sdf.format(new Date(elapsedTime)));
	}
	
	//this method is called whenever the status of a square is changed
	private void updateHints(int row, int col){
		Cell square = squares[row][col];

		int status = square.getStatus();
		
		rowHints[row].setSquare(col, status);
		colHints[col].setSquare(row, status);
				
		winCheck();
	}
	
	public void winCheck(){
		boolean isCorrect = true;
		
		if(fillCounter == puzzle.getMagicNumber()){
			for(int i = 0; i < height && isCorrect; i++){
				isCorrect = rowHints[i].checkCorrectness();
			}
			
			if(isCorrect){
				gameWon = true;
				puzzle.setFinished(true);
				
				System.out.println(puzzle.getName() + ", " + puzzle.getFinished());
				gameGrid.setVisible(false);
				Image i = new Image("file:images/puzzle" + puzzle.getID() + ".png", 250, 250, true, true);
				image.setFitHeight(250);
				image.setFitWidth(250);
				image.setImage(i);
				backToMenu();
			}
		}
	}
	
	public void leftClick(Cell square){
		int currentStatus = square.getStatus();
		
		if(currentStatus == BLANK){
			square.setStatus(FILL);
			fillCounter++;
		}
		else if(currentStatus == FILL){
			square.setStatus(BLANK);
			fillCounter--;
		}
		else{
			square.setStatus(BLANK);
		}
	}
	
	public void rightClick(Cell square){
		int currentStatus = square.getStatus();

		if(currentStatus == BLANK){
			square.setStatus(X);
		}
		else if(currentStatus == FILL) {
			square.setStatus(BLANK);
			fillCounter--;
		}
		else{
			square.setStatus(BLANK);
		}
	}
	
	private void highlight(Cell square){
		int r = square.getRow();
		int c = square.getCol();
		
		rowHints[r].setStyle("-fx-background-color:yellow");
		colHints[c].setStyle("-fx-background-color:yellow");
	}
	
	private void unhighlight(Cell square){
		int r = square.getRow();
		int c = square.getCol();
		
		rowHints[r].setStyle("-fx-background-color:white");
		colHints[c].setStyle("-fx-background-color:white");
	}
	
}
